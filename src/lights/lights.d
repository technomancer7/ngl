import std.stdio, tcm.opthandler, std.algorithm, std.array, std.string, std.process, std.conv, tcm.colours;
import tcm.figlet, tcm.randplus;

void main(string[] argv){
  auto o = new Opt(argv);
  string line;
  //executeShell("clear").output.writeln;
  figlet("lights");
  auto g = new LightsLinear(to!int(o.value("length", "8")), o.flag("loop"), o.flag("randoms"), o.value("odds", "50").to!int);
  writeln("Options:\nlength "~to!string(g.lights.length)~"\nloop "~to!string(g.loop)~"\nrandoms "~to!string(g.randoms)~"\nodds "~o.value("odds", "50")~"\n");
  writeln("Enter index numbers to flip the lights:");
  g.outputLights.writeln;
  while((line = readln()) !is null){
    line = line.stripRight("\n");
    g.flipLight(to!int(line));
    g.outputLights.writeln;
  }
}
class LightsLinear {
  bool[] lights;
  bool loop;
  bool randoms;
  int rnum;
  int rdone;
  
  this(int len = 8, bool loop = false, bool randoms = false, int odds = 50){
    this.loop = loop;
    this.randoms = randoms;
    if(len < 5) len=5;
    this.rnum = randrange(1, len/3);
    this.rdone = 0;
    for(int i = 0; i < len; i++) {
	bool done;
	if(randoms){
	  if(rdone <= rnum){
	    int c = randrange(0, 100);
	    if(c >= odds){
		this.lights ~= true;
		this.rdone++;
		done = true;
	    }
	  }
	}

	if(!done)
	this.lights ~= false;
    }
  }
  
  void flipLight(int index){
    if(index >= this.lights.length){"Index out of bounds...".writeln; return;} 
    if(index == 0){
	if(this.loop){
	  this.lights[$-1] = !this.lights[$-1];
	}
    } else this.lights[index-1] = !this.lights[index-1];
    
    this.lights[index] = !this.lights[index];
    if(index == this.lights.length-1){
	if(this.loop){
	  this.lights[0] = !this.lights[0];
	}
    } else this.lights[index+1] = !this.lights[index+1];
  }

  string outputLights(string onString = "[X]", string offString = "[ ]", string seperator = " ", string ruler = " * "){
    string o;
    string r;
    int i;

    foreach (l; this.lights){
	o ~= seperator;
	r ~= seperator;
	r ~= ruler.replace("*", to!string(i));
	if(l){
	  o ~= onString;
	} else o ~= offString;
	    i++;
    }
    o ~= seperator;
    r ~= seperator;

    return r~"\n"~o;
  }
}
