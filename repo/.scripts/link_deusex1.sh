#!/bin/bash
path1="$HOME/.wine/drive_c/Deus Ex GOTY/"
echo "Checking $path1"
fpath=""
if [ -d "$path1" ]; then
    echo "Found an existing installation at $path1"
    fpath="$path1"
fi

read -p "Path to Deus Ex root directory ($fpath): " chkpath
fpath=${chkpath:-$fpath}
echo "Using $fpath"

read -p "Launcher name (DeusEx.exe): " chkfile
chkfile=${chkfile:-DeusEx.exe}
launcher="$(printf "$fpath%s" "System/$chkfile")"
echo "Checking $launcher"

if [ ! -f "$launcher" ]; then
    echo "Launcher EXE not found here."
else
    echo "Launcher OK"
    read -p "Wine executable (wine): " winebin
    winebin=${winebin:-wine}
    cdrv="C:/"
    rd="$HOME/.wine/drive_c/"
    echo "$rd"
    outp=${$launcher/$rd/$cdrv}
    echo "outp $outp"
    
    fullcmd="$winebin \"$launcher\""
    
    echo "Full command: $fullcmd"
    #cd "$fpath"
    #wine $launcher
fi
