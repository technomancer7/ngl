echo "Dowloading GOLANG $1..."
wget https://golang.org/dl/go$1.linux-amd64.tar.gz

echo "Uninstalling old version..."
sudo rm -r /usr/local/go/

echo "Unpacking go$1.linux-amd64.tar.gz"
sudo tar -C /usr/local -xzf go$1.linux-amd64.tar.gz

echo "Testing version installation..."
go version
rm go$1.linux-amd64.tar.gz
