require 'faraday'
require 'json'
cfg = JSON.parse(File.read("#{Dir.home}/.ngl.d/configs/data.json"))["global"]
arch = cfg["arch"]
go_os = cfg["go_os"]
vers = cfg["go_version"]

url = "https://golang.org/dl/go#{vers}.#{go_os}.tar.gz"

unless File.exists? "#{Dir.home}/.ngl.d/repo/go#{vers}.#{go_os}.tar.gz"
    `wget -P ~/.ngl.d/repo/ #{url}`
end
`sudo rm -r /usr/local/go/`
`sudo tar -C /usr/local -xzf ~/.ngl.d/repo/go#{vers}.#{go_os}.tar.gz`
`go version`
`rm go#{vers}.#{go_os}.tar.gz`
