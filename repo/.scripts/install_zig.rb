require 'faraday'
require 'json'
cfg = JSON.parse(File.read("#{Dir.home}/.ngl.d/configs/data.json"))["global"]
arch = cfg["arch"]
vers = cfg["zig_version"]
rp = Faraday.get "https://ziglang.org/download/index.json"
js = JSON.parse(rp.body)
url = js[js.keys[1]]["#{arch}-linux"]["tarball"]
unless File.exists? "#{Dir.home}/.ngl.d/repo/zig-linux-#{arch}-#{vers}.tar.xz"
`wget -P ~/.ngl.d/repo/ #{url}`
end
`tar -C ~/ -xf ~/.ngl.d/repo/zig-linux-#{arch}-#{vers}.tar.xz`
`mv ~/zig-linux-#{arch}-#{vers} ~/zig`
