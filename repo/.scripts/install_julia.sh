#!/bin/bash

echo "Downloading..."
wget -P ~/.ngl.d/repo/ https://julialang-s3.julialang.org/bin/linux/x64/1.5/julia-$1-linux-x86_64.tar.gz
rm -r ~/julia/

echo "Extracting..."
tar -C ~/ -xzf ~/.ngl.d/repo/julia-$1-linux-x86_64.tar.gz
mv ~/julia-$1 ~/julia
rm ~/.ngl.d/repo/julia-$1-linux-x86_64.tar.gz
